# #Creating a list
colors=[]

# #Add Elements in list
 colors.append('red')
 colors.append('green')
 colors.append('black')
 colors.append([0,1,2])

# #Change Elements in list
# colors[1]='gray'
#remove elements from last location
colors.pop()
print(colors)
#Add Elements at aspecific location
colors.insert(2,'white')
#remove elements at specific location
colors.remove('white')
#del color
print('Before',colors)
del colors[1]
print('After',colors)
print(len(colors))